namespace :choique do

  # CHOIQUE_MIGRATE_TMP_DIR = ENV.fetch('CHOIQUE_ASSETS_DATA_DIR') do
  #   raise 'Missing env variable CHOIQUE_ASSETS_DATA_DIR with Choique assets'
  # end
  #CHOIQUE_MIGRATE_TMP_DIR = 'data/choique/migrate'
  #CHOIQUE_MIGRATE_TMP_DIR = '../econo/opt/symfony-applications/shared/uploads'
  #BASE_DIR = Rails.root.join CHOIQUE_MIGRATE_TMP_DIR


  CHOIQUE_MIGRATE_TMP_DIR = ENV.fetch('CHOIQUE_ASSETS_DATA_DIR') do
    raise 'Missing env variable CHOIQUE_ASSETS_DATA_DIR with Choique assets'
  end
  BASE_DIR = Rails.root.join CHOIQUE_MIGRATE_TMP_DIR

  FORCE_MIGRATE_UNPUBLISHED = ENV.fetch('FORCE_MIGRATE_UNPUBLISHED') { false }

  desc 'Flush data stored from previous migrations'
  task :flush => :environment do |t, args|
    ChoiqueMigrate::ChoiqueYavuArticle.delete_all
    ChoiqueMigrate::ChoiqueYavuSection.delete_all
    puts "Choique previuous data migrated flushed"
  end

  desc 'Sync assets from choique server'
  task :sync, [:server, :remote_path] do |t, args|
    server = args[:server]
    remote_path = args[:remote_path]
    raise ':server argument must be set' unless server
    raise ':remote_path argument must be set' unless remote_path
    puts "Creating base directory #{dir}"
    %w{assets docs}.each do |x|
      %x{mkdir -p #{BASE_DIR}/#{x}}
      puts "Syncing remote #{x} into #{dir}"
      %x{rsync -a #{server}:#{remote_path}/#{x}/ #{dir}/#{x}}
    end
  end

  desc "Migrate from choique instance: migration can be imanavged by arguments [news_source,supplement,limit], they both defaults to choique"
  task :migrate, [:news_source, :supplement, :limit, :show_console] => :environment do |t, args|
    Article.skip_callback(:save, :before, :update_time)
    args.with_defaults(news_source: 'choique', supplement: 'choique', show_console: true)
    report_builder = ReportBuilder.new(args[:show_console]) #si no se manda es false o nil?
    ensure_needed_extra_attributes
    migrate_section_articles report_builder, news_source_name: args[:news_source], supplement_name: args[:supplement], limit: args[:limit]
    #handle_results report_builder
    Article.set_callback(:save, :before, :update_time)
  end

  def ensure_needed_extra_attributes
    ExtraAttribute.find_or_create_by!(name: 'is_news') do |ea|
      ea.description    = '¿Es una novedad?'
      ea.attribute_type = 'boolean'
      ea.model_classes  = { 'article' => '1' }
    end
  end

  desc "Relate media with articles. Run after data is migrated"
  task :relate, [:limit] => :environment do |t, args|
    Article.skip_callback(:save, :before, :update_time)
    relate_article_media limit: args[:limit]
    Article.set_callback(:save, :before, :update_time)
  end

  desc "Readapt references to media, documents, galleries, etc. inside article's body"
  task  :readapt, [:limit] => :environment do |t, args|
    Article.skip_callback(:save, :before, :update_time)
    readapt_articles_body limit: args[:limit]
    Article.set_callback(:save, :before, :update_time)
  end

  desc "Recover choique bodies"
  task :recover => :environment do |t, args|
    Article.skip_callback(:save, :before, :update_time)
    ChoiqueMigrate::ChoiqueYavuArticle.all.each do |cy_article|
      c_body = ChoiqueMigrate::Article.find_by(id: cy_article.choique_id).body
      y_article = Article.find_by(id: cy_article.yavu_id)
      y_article.body = c_body
      y_article.save!
    end
    Article.set_callback(:save, :before, :update_time)
  end

  desc "Load events associated with articles"
  task :load_events => :environment do |t, args|
    Article.skip_callback(:save, :before, :update_time)
    missing_event_fields = %w(event_unpublished event_name event_place event_date event_time) - ExtraAttribute.for(Article).map(&:name)
    if missing_event_fields.any?
      puts "Faltan estos campos extras en Articulo para poder migrar los eventos: #{missing_event_fields}"
      exit 1
    end
    migrated = 0
    ChoiqueMigrate::Event.all.each do |choique_event|
      next unless choique_event.article_id
      choique_migrated_article = ChoiqueMigrate::ChoiqueYavuArticle.find_by(choique_id: choique_event.article_id)
      if choique_migrated_article
        yavu_article = Article.find_by(id: choique_migrated_article.yavu_id)
        yavu_article.extra_attribute_model.event_unpublished = !choique_event.is_published
        yavu_article.extra_attribute_model.event_name        = choique_event.title
        yavu_article.extra_attribute_model.event_place       = choique_event.location
        yavu_article.extra_attribute_model.event_date        = choique_event.begins_at.to_date.to_s
        yavu_article.extra_attribute_model.event_time        = choique_event.begins_at.to_time.to_s
        yavu_article.save!
        migrated += 1
      end
    end
    puts "Se han migrado #{migrated} eventos de un total de #{ChoiqueMigrate::Event.count}"
    Article.set_callback(:save, :before, :update_time)
  end

  def media_types
    { external:  'EmbeddedMedium',
      image:     'ImageMedium',
      audio:     'AudioMedium',
    }
  end

  def build_yavu_medium(medium, klass, type)
    if File.exists?(build_path(type, medium))
      find_and_save_medium medium, klass, type
    else
      raise Errno::ENOENT
    end
  end

  def choique_medium_lookup(medium_type, something)
    choique_medium_id = if something.is_a?(ChoiqueMigrate::Multimedia)
      something.id
    else
      something # Must be the id
    end

    "#{ChoiqueMigrate.database_name}-#{medium_type}:#{choique_medium_id}"
  end

  def find_and_save_medium(c_medium, klass, type)
    credits_type = (type == 'document')? 'document' : 'multimedia'
    medium =  klass.constantize.find_or_initialize_by(credits: choique_medium_lookup(credits_type, c_medium))
    save_medium medium, c_medium, type unless medium.persisted?
  end

  def save_medium(medium, c_medium, type)
    medium.name = c_medium.title

    if(type == 'external')
      medium.content = c_medium.external_uri
    else
      medium.file = ::Pathname.new(build_path(type, c_medium)).open
    end

    medium.save!
  rescue Errno::ENAMETOOLONG
    #FIXME: Considerar esto para el reporte
    puts "Nombre muy largo"
  end

  def build_path(type, medium)
    if (type == 'document')
      medium.uri.empty? ? '' :  BASE_DIR.join('docs', medium.uri)
    else
      BASE_DIR.join('assets', medium.large_uri)
    end
  end

  def migrate_section_articles(report_builder, news_source_name: 'choique', supplement_name: 'choique', limit: nil)
    news_source = NewsSource.find_or_create_by! name: news_source_name
    supplement = Supplement.find_or_create_by! news_source: news_source, name: supplement_name
    edition = Edition.find_or_create_by! name: news_source_name,  supplement: supplement, is_active: false
    supplement.editions << edition unless supplement.editions.include?(edition)

    migrate_sections(supplement, edition, limit, report_builder)
  end

  def migrate_sections(supplement, edition, limit, report_builder)
    report_builder.print_console("\nMigrating #{ChoiqueMigrate::Section.count} Sections (choique_id - title)\n")

    ChoiqueMigrate::Section.order(section_id: :asc).each.with_index do |c_section, index|
      begin
        report_builder.print_console("#{c_section.id} - #{c_section.title}\n".indent(2))
        if c_section.is_published? || FORCE_MIGRATE_UNPUBLISHED
          migrate_articles c_section, build_section(c_section, supplement, edition), edition, limit, report_builder
        else
          report_builder.add_unpublished_section c_section
        end
      rescue ActiveRecord::RecordInvalid
        report_builder.add_error_section c_section
      end
    end
    migrate_parents
  end

  def build_section(c_section, supplement, edition)
    cy_section = ChoiqueMigrate::ChoiqueYavuSection.find_or_initialize_by(choique_id: c_section.id)
    if cy_section.yavu_id.present?
      Section.find_by(id: cy_section.yavu_id)
    else
      begin
        create_section cy_section, c_section, supplement, edition
      rescue NameError::NoMethodError
        handle_priority_error "sección"
      end
    end
  end

  def handle_priority_error(type)
    puts "\n#{">"*40}#{"<"*40}"
    puts "WARNING"
    puts "priority is not defined. This 'Atributo Extra Personalizado' must be added to continue:"
    puts "Clave: priority\nDescripcion: prioridad\nTipo: String\nDisponible para: #{type}"
    puts "#{">"*40}#{"<"*40}\n"
    exit
  end

  def build_article(c_article, section, edition)
    cy_article = ChoiqueMigrate::ChoiqueYavuArticle.find_or_initialize_by(choique_id: c_article.id)
    create_article c_article, cy_article, section, edition unless cy_article.yavu_id.present?
  end

  def migrate_articles(c_section, section, edition, limit, report_builder)
    articles = limit.nil? ? c_section.articles : c_section.articles.limit(limit)
    report_builder.print_console "Migrating #{articles.count} articles #{limit.nil? ? '': '(limited to '+limit+')'} ".indent(4)
    articles.each do |c_article|
      begin
        if c_article.is_published? || FORCE_MIGRATE_UNPUBLISHED
          build_article c_article, section, edition
          migrate_media_from c_article, report_builder
        else
          report_builder.add_unpublished_article c_article
        end
      rescue ActiveRecord::RecordInvalid
        report_builder.add_error_article c_article, c_section
      end
    end
  end

  def migrate_media_from(c_article, report_builder)
    migrate_multimedia c_article.multimedia, media_types[c_article.multimedia.type.to_sym], c_article.multimedia.type, report_builder, c_article unless c_article.multimedia.nil?

    c_article.multimedias.each do | multimedia |
      migrate_multimedia multimedia, media_types[multimedia.type.to_sym], multimedia.type, report_builder, c_article
    end

    c_article.documents.each do | document |
      migrate_multimedia document, 'FileMedium','document', report_builder, c_article
    end

    if c_article.main_gallery
      c_article.main_gallery.multimedia.each do | multimedia |
        migrate_multimedia multimedia, media_types[multimedia.type.to_sym], multimedia.type, report_builder, c_article
      end
    end

    c_article.galleries.each do | gallery |
      gallery.multimedia.each do | multimedia |
        migrate_multimedia multimedia, media_types[multimedia.type.to_sym], multimedia.type, report_builder, c_article
      end
    end
  end

  def migrate_multimedia(multimedia, klass, type, report_builder, article)
    begin
      build_yavu_medium(multimedia, klass, type) unless type.nil? || klass.nil?
    rescue ActiveRecord::RecordInvalid
      report_builder.add_article_media multimedia, article, "Record Invalid"
    rescue Errno::ENOENT
      report_builder.add_article_media multimedia, article, "File not found"
    end
  end

  def create_section(cy_section, c_section, supplement, edition)
    s = Section.new
    s.name = c_section.title
    s.slug = c_section.name.parameterize
    s.supplement = supplement
    s.is_visible = c_section.is_published
    s.extra_attribute_model.color = c_section.color
    s.extra_attribute_model.priority = c_section.priority
    s.main_article = main_article_for(c_section, s, edition)

    s.save!
    s.reload

    cy_section.yavu_id = s.id
    cy_section.save!

    s
  end

  def main_article_for(c_section, y_section, edition)
    c_article = ChoiqueMigrate::Article.find_by(id: c_section.article_id) if c_section.article_id
    build_article c_article, y_section, edition if c_article
  end

  def migrate_parents
    ChoiqueMigrate::ChoiqueYavuSection.all.each do | cy_section |
      c_parent = ChoiqueMigrate::Section.find_by(id: cy_section.choique_id).section
      if c_parent
        y_parent = ChoiqueMigrate::ChoiqueYavuSection.find_by(choique_id: c_parent.id)
        if y_parent
          y_section = Section.find_by(id: cy_section.yavu_id)
          unless y_section.parent
            y_section.parent = Section.find_by(id: y_parent.yavu_id)
            y_section.save!
          end
        end
      end
    end
  end

  def create_article(c_article, cy_article, section, edition)
    a = Article.new
    a.section = section
    a.edition = edition
    a.is_new = c_article.type == 1 ? true : false
    a.slug = c_article.name.parameterize
    a.title  = c_article.title
    a.heading = c_article.upper_description
    a.lead = c_article.heading
    a.body = c_article.body
    a.is_visible = c_article.is_published
    a.time = c_article.published_at || c_article.created_at
    a.created_at = c_article.created_at
    a.extra_attribute_model.is_news = c_article.type == 1

    a.save!

    cy_article.yavu_id = a.id
    cy_article.save!
    a
  end

  def relate_article_media(limit: nil)
    articles = ChoiqueMigrate::Article.all
    articles = articles.limit(limit) if limit
    print "Creating media associations and galleries for #{limit ? '': "#{limit} of "}#{articles.count} articles:\n"

    articles.each do | c_article |
      yavu_id = ChoiqueMigrate::ChoiqueYavuArticle.find_or_initialize_by(choique_id: c_article.id).yavu_id
      y_article = Article.find_by(id: yavu_id)

      if y_article
        errors = {media: [], galleries: {}, articles: [], documents: []}
        create_article_media y_article, c_article, errors
        relate_articles y_article, c_article, errors
        print_relate_media_errors errors, c_article
      end
    end
  end

  def readapt_articles_body(limit: nil)
    articles = ChoiqueMigrate::ChoiqueYavuArticle.all
    articles = articles.limit(limit) if limit
    print "Readapt body for #{limit ? '': "#{limit} of "}#{articles.count} articles:\n"
    articles.each do | cy_article |
      article = Article.find(cy_article.yavu_id)
      body = article.body.gsub(/\{{(.*?)\}}|\[\[(.*?)\]\]/) do |match|
        build_tag match, article
      end if article.body
      article.update!(body: body)
    end
  end

  def create_article_media(y_article, c_article, errors)
    relate_medium c_article.multimedia, y_article, errors, true, 0, 'multimedia', false  if c_article.multimedia.present?

    order = relate_media c_article.multimedias, y_article, errors, 1
    order = relate_media c_article.documents, y_article, errors, order, 'document'
    relate_galleries  y_article, c_article, errors, order
  end

  def relate_media(collection, y_article, errors, order, type = 'multimedia', is_gallery = false)
    collection.each do | c_medium |
      relate_medium c_medium, y_article, errors, false, order, type, is_gallery
      order = order + 1
    end
    order
  end

  def relate_medium(c_medium, y_article, errors, main, order, type, is_gallery)
    y_medium  = Medium.find_by credits: choique_medium_lookup(type, c_medium)
    if y_medium
       relation = ArticleMedium.find_or_create_by(article_id: y_article.id, medium_id:  y_medium.id)
       relation.main = main
       relation.order = order
       relation.caption = c_medium.title
       relation.for_gallery = is_gallery
       relation.save!
       
       news_source_relation = MediumNewsSource.find_or_create_by(news_source_id: y_article.news_source.id , medium_id:  y_medium.id)
       news_source_relation.save!
    else
      errors[:media] << "#{c_medium.id} -  #{ c_medium.title }"
    end
  end

  def relate_galleries(y_article, c_article, errors, order)
    order = relate_media c_article.main_gallery.multimedia, y_article, errors, order, 'multimedia', true unless c_article.main_gallery.nil?
    c_article.galleries.each do | c_gallery |
      order = relate_media c_gallery.multimedia, y_article, errors, order, 'multimedia', true
    end
  end

  def print_indented_errors(errors, msg, indentation)
    if errors.any?
      puts msg
      puts (errors.join "\n").indent(indentation)
      puts
    end
  end

  def print_gallery_errors(gallery_erros)
    if errors[:galleries].any?
      puts "WARNING: Media from galleries doesn't exist in Yavu: \n"
      errors[:galleries].each do |gallery, errors|
        puts "Gallery #{gallery}".indent(5)
        puts (errors.join "\n").indent(7)
      end
    end
  end

  def print_relate_media_errors(errors, c_article)
    if errors[:media].any? || errors[:galleries].any? || errors[:articles].any? || errors[:documents].any?
      puts "#{">"*50}#{"<"*50}"
      puts "Article '#{c_article.title}'"

      print_indented_errors(errors[:media], "WARNING: Media doesn't exist in Yavu: \n", 5)
      print_indented_errors(errors[:articles], "WARNING: Articles associated doesn't exist in Yavu: \n", 5)
      print_indented_errors(errors[:documents], "WARNING: Documents doesn't exist in Yavu: \n", 5)
      puts "#{">"*50}#{"<"*50}"
      puts
    end
  end

  def relate_articles(referrer_article, c_article, errors)
    c_article.articles.each do | article |
      cy_article = ChoiqueMigrate::ChoiqueYavuArticle.find_or_initialize_by(choique_id: article.id)
      referee_article = Article.find_by(id: cy_article.yavu_id)

      if referee_article
        create_article_article referrer_article, referee_article
      else
        errors[:articles] << "#{article.id} -  #{ article.title }"
      end
    end
  end


  def create_article_article(referrer, referee)
    ArticleRelations.find_or_create_by(article_a: referrer, article_b: referee)
  end


  def build_tag(match, article)
    type =  match.split(/[^\w-]+/)[1]
    choique_id =  match.split(/[^\w-]+/)[2]
    caption = match.split(/\|(.*?)\]|\|(.*?)\}/)[1]
    if (type == "multimedia")
      type = ChoiqueMigrate::Multimedia.find_by(id: choique_id).type
    end

    attributes = {choique_id: choique_id, type: type, article_id: article.id, caption: caption }

    case type
      when  /choique_\w+/, /article_media/
        match
      when 'image', 'audio', 'documento'
        replace_medium attributes
      when 'external'
        text = replace_medium  attributes
        "<div class='video_iframe'>#{text}</div>"
      when 'articulo'
        replace_article attributes
      when 'galeria'
        "{{choique_gallery:#{choique_id} no puede crearse en Yavu. Los medios correspondientes fueron agregados a la colección
        de medios del artículo.}}"
      else
        replace_text "choique_#{type}", choique_id
    end
  end

  def replace_article(**attributes)
    choique_yavu_article = ChoiqueMigrate::ChoiqueYavuArticle.find_by(choique_id: attributes[:choique_id])
    referee_id = choique_yavu_article.yavu_id if choique_yavu_article

    if choique_yavu_article
      create_article_relation(attributes[:article_id], referee_id, attributes[:caption])
      replace_text("article", referee_id)
    else
      replace_text("choique_article", attributes[:choique_id])
    end
  end


  def replace_medium(**attributes)
    type = (attributes[:type] == "documento" ? "document" : "multimedia")
    yavu_medium = Medium.find_by credits: choique_medium_lookup(type, attributes[:choique_id])

    if yavu_medium
      begin
        relation = create_medium_caption(attributes.merge({medium_id: yavu_medium.id}))
        replace_text("article_media", relation.id)
      rescue ActiveRecord::RecordInvalid
        #handle?
      end
    else
      replace_text("choique_#{type}", attributes[:choique_id])
    end
  end

  def create_medium_caption(**attributes)
    check_presence_of :article_id, :medium_id, in_hash: attributes
    ArticleMedium.find_or_create_by(attributes.slice(:article_id, :medium_id)).tap do |relation|
      caption = attributes[:caption].empty? ? Medium.find_by(id: attributes[:medium_id]).name : attributes[:caption]
      relation.caption = caption
      relation.save!
    end
  end

  def check_presence_of(*attributes, in_hash:)
    blank_attributes = attributes.select { |attr| in_hash[attr].blank? }
    if blank_attributes.any?
      raise RuntimeError.new("Error #{caller[0]} -> Must receive a value of #{blank_attributes.join(', ')}")
    end
    true
  end

  def create_article_relation(referrer, referee, caption)
    begin
      relation = ArticleRelations.find_or_create_by(article_a_id: referrer, article_b_id: referee)
      relation.caption = caption.empty? ? Article.find_by(id: referee).title : caption
      relation.save!
    rescue ActiveRecord::RecordInvalid
      #handle?
    end
  end

  def replace_text(type, id)
    "{{#{type}:#{id}}}"
  end

  def handle_results(report_builder)
    #throw report_builder.report
    create_file
    send_file
  end

  def create_file(results)

    #File.open('/tmp/migracion_#{DateTime.now.strftime("%d-%m-%Y")}.txt', 'w') do | file  |
    #  file.puts "Resultados\nMigracion"
    #end
  end

  def send_file
    ChoiqueMigrate::ReportMailer.report_email(
      from: 'rosariosantamarina1@gmail.com',
      to: 'rsantamarina@cespi.unlp.edu.ar',
      subject: 'reporte')
    .deliver
  end

end

