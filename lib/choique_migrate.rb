require "choique_migrate/engine"

module ChoiqueMigrate
  def self.db_connection
    db_conf = YAML::load(File.open(File.join(Rails.root,'config','database.yml')))
    db_conf = db_conf["choique"][Rails.env]
    if db_conf.has_key?('url')
      db_conf['url']
    elsif db_conf.has_key?(:url)
      db_conf[:url]
    else
      db_conf
    end
  end

  def self.database_name
    db_connection['database']
  end
end
