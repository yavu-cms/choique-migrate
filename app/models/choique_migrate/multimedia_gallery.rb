module ChoiqueMigrate
  class MultimediaGallery < ActiveRecord::Base
    establish_connection ChoiqueMigrate.db_connection
    self.table_name = 'multimedia_gallery'
   
    belongs_to :gallery
    belongs_to :multimedia   

  end
end
