module ChoiqueMigrate
  class Article < ActiveRecord::Base
    establish_connection ChoiqueMigrate.db_connection
    self.table_name = 'article'
    self.inheritance_column = :_type_disabled

    belongs_to :section
    belongs_to :multimedia
    belongs_to :main_gallery, class_name: "Gallery"
    has_and_belongs_to_many :multimedias, join_table: 'article_multimedia'

    has_many :article_galleries
    has_many :galleries, through: :article_galleries
    has_and_belongs_to_many :documents, join_table: 'article_document'
    has_and_belongs_to_many :articles, class_name: "Article", foreign_key: "article_referer_id", association_foreign_key: "article_referee_id",  join_table: 'article_article'
  end
end