module ChoiqueMigrate
  class EventType < ActiveRecord::Base
    establish_connection ChoiqueMigrate.db_connection
    self.table_name = 'event_type'
    self.inheritance_column = :_type_disabled
  end
end