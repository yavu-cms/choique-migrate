module ChoiqueMigrate
  class Event < ActiveRecord::Base
    establish_connection ChoiqueMigrate.db_connection
    self.table_name = 'event'
    self.inheritance_column = :_type_disabled

    belongs_to :article
    belongs_to :event_type
  end
end