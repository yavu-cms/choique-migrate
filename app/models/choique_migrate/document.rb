module ChoiqueMigrate
  class Document < ActiveRecord::Base
    establish_connection ChoiqueMigrate.db_connection
    self.table_name = 'document'
    self.inheritance_column = :_type_disabled  

  end
end
