module ChoiqueMigrate
  class Gallery < ActiveRecord::Base
    establish_connection ChoiqueMigrate.db_connection
    self.table_name = 'gallery'
    
    has_many :multimedia_galleries
    has_many :multimedia, through: :multimedia_galleries

    has_many :article_galleries
    has_many :articles, through: :article_galleries
  
  end
end

