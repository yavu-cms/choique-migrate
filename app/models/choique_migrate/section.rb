module ChoiqueMigrate
  class Section < ActiveRecord::Base
    establish_connection ChoiqueMigrate.db_connection
    self.table_name = 'section'
    self.inheritance_column = :_type_disabled

    belongs_to :main_article, class_name: 'ChoiqueMigrate::Article', foreign_key: 'article_id'
    has_many :articles
    belongs_to :section
    has_many :sections
    has_and_belongs_to_many :documents, join_table: 'section_document'
  end
end