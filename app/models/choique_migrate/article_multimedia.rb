module ChoiqueMigrate
  class ArticleMultimedia < ActiveRecord::Base
    establish_connection ChoiqueMigrate.db_connection
    self.table_name = 'article_multimedia'
    self.inheritance_column = :_type_disabled

    belongs_to :article
    belongs_to :multimedia
  end
end