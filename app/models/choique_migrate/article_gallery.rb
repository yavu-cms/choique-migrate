module ChoiqueMigrate
  class ArticleGallery < ActiveRecord::Base
    establish_connection ChoiqueMigrate.db_connection
    self.table_name = 'article_gallery'

    belongs_to :gallery
    belongs_to :article

  end
end



