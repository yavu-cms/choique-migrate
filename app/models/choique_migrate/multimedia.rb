module ChoiqueMigrate
  class Multimedia < ActiveRecord::Base
    establish_connection ChoiqueMigrate.db_connection
    self.table_name = 'multimedia'
    self.inheritance_column = :_type_disabled

    has_many :multimedia_galleries
    has_many :galleries, through: :multimedia_galleries

  end
end
