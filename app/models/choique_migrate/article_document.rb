module ChoiqueMigrate
  class ArticleDocument < ActiveRecord::Base
    establish_connection ChoiqueMigrate.db_connection
    self.table_name = 'article_document'
    self.inheritance_column = :_type_disabled

    belongs_to :article
    belongs_to :document
  end
end