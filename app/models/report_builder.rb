class ReportBuilder
  SECTION_IDENTATION = 4
  ARTICLE_IDENTATION = 6
  MEDIA_IDENTATION   = 8

  attr_accessor :report
  attr_accessor :show_console

  def initialize(show_console)
    @report = {
     	sections: {
     		error: {},
     		unpublished: {}
     	},
     	articles: {
     		error: {},
     		unpublished: {}
     	},
    	media: {}
    }
    @show_console = show_console
  end


  def add_unpublished_section(section)
    add_unpublished(section, :sections, SECTION_IDENTATION)
  end

  def add_unpublished_article(article)
    add_unpublished(article, :articles, ARTICLE_IDENTATION)
  end

  def add_unpublished(object, type, identation)
    @report[type][:unpublished][object.id] = object.title
    print_console "WARNING - #{object.id} - '#{object.title}' won't be migrated because is unpublished\n".indent(identation)
  end

  def add_error_section(section)
    if @report[:sections][:error].has_key?(section.id)
      @report[:sections][:error][section.id].merge(build_error_section(section))
    else
      @report[:sections][:error][section.id] = build_error_section(section)
    end
    print_console "WARNING - #{section.id} - '#{section.title}' won't be migrated because an error ocurred. Rename its slug.\n".indent(SECTION_IDENTATION)
  end

  def add_error_article(article, section)
    if @report[:articles][:error].has_key?(section.id)
      @report[:articles][:error][article.id].merge(build_error_article(article, section))
    else
      @report[:articles][:error][article.id] = build_error_article(article, section)
    end
    print_console "WARNING - #{article.id} - '#{article.title}' won't be migrated because an error ocurred.".indent(ARTICLE_IDENTATION)
  end

  def add_article_media(multimedia, article, exception_type)
    if @report[:media].has_key?(multimedia.id)
      @report[:media][multimedia.id].merge(name: multimedia.name, article: article.title)
    else
      @report[:media][multimedia.id] = {name: multimedia.name, article: article.title}
    end
    print_console "#{article.id} - '#{article.title}' multimedia:".indent(ARTICLE_IDENTATION)
    print_console "WARNING - #{multimedia.id} - '#{multimedia.name}' won't be migrated because #{exception_type}.".indent(MEDIA_IDENTATION)
  end

  def build_error_section(section)
    {
    	title: section.title,
      articles: build_section_articles(section.main_article, section.articles)
    }
  end

  def build_error_article(article, section)
    data = build_article article
    data[:section] = {id: section.id, title: section.title}
  end

  def build_article(article)
    {
      title: article.try(:title) || 'Articulo desconocido',
      media: build_article_media(article)
    }
  end

  def build_media(media)
    {
      id: media.id,
      name: media.name
    }
  end

  def build_section_articles(main_article, articles)
    result = []
    result << build_article(main_article)
    articles.each do | article |
      result << build_article(article)
    end
  end

  def build_article_media(article)
    result = {}
    if article
      result[article.multimedia.type.to_sym] = build_media(article.multimedia) if article.multimedia

      article.multimedias.each do | multimedia |
        result[multimedia.type.to_sym] = build_media(multimedia)
      end

      article.documents.each do | document |
        result[:document] = build_media(document)
      end
    else
      puts 'No hay articulo para construir reporte de errores de medios'
    end
  end

  def print_console(msg)
    puts msg if @show_console
  end

  def print_unpublished_sections
    print_unpublished :sections
  end

  def print_unpublished_sections
    print_unpublished :articles
  end

  def print_unpublished(type)
    #falta el output
    puts "Las siguientes #{type} no se migraron por estar DESPUBLICADAS\n"
    @report[:type][:unpublished].each do | e |
      puts "Choique id #{e[:id]} - #{e[:title]}\n"
    end
  end
end



#print_errors(multimedia_errors, "WARNING: Media from article #{c_article.name} cant be migrated. Choique names are:")
#    print_errors(document_errors, "WARNING: Media from article #{c_article.name} cant be migrated. Choique documents names are:")

 #print ("% 10d articles to migrate #{limit.nil? ? '': '(limited to '+limit+')'}: " % articles.count)

  #    "\nMigrating media from article #{c_article.name}:"