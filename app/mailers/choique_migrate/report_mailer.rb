module ChoiqueMigrate
  class ReportMailer < ActionMailer::Base
    default from: "from@example.com"
    default content_type: 'text/plain'

    def sendmail(from:, to:, subject:, body:)
      mail(from: from, reply_to: from, to: to, body: body, subject: subject).deliver
    end

    def report_email(from:, to:, subject:)
      attachments['text.txt'] = File.read('text.txt')
      mail(from: from, reply_to: from, to: to, subject: subject)
    end

  end
end