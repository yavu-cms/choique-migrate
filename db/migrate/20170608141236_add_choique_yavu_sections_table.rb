class AddChoiqueYavuSectionsTable < ActiveRecord::Migration
  def change
  	create_table :choique_yavu_sections, :id => false do |t|
      t.integer :choique_id
      t.integer :yavu_id
    end
  end
end
