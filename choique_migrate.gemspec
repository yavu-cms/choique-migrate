$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "choique_migrate/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "choique_migrate"
  s.version     = ChoiqueMigrate::VERSION
  s.authors     = ["Christian A. Rodriguez"]
  s.email       = ["car@cespi.unlp.edu.ar"]
  s.homepage    = "http://www.cespi.unlp.edu.ar"
  s.summary     = "Migarte from choique CMS instance"
  s.description = "Migrate Sections, articles and medias from Choique CMS"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.13"
end
