# Migracion de choique a Yavu

La migración de choique a Yavu considera únicamente:

* Migración de secciones
* Migración de artículos
* Migración de medios: embebidos, imágenes y documentos

## Instalando el plugin

Agregar al `Gemfile.extensions` la siguiente línea

```ruby
gem 'choique_migrate', git: 'https://git.cespi.unlp.edu.ar/desarrollo/yavu-choique-migrate.git'
```

en el caso de trabajar en desarrollo, cambiar la línea antes mencionada por:

```ruby

gem 'choique_migrate', path: 'PATH/A/LA/GEMA'

```

Correr `bundle update`

## Pasos para migrar

Antes de comenzar debemos agregar la configuracion de la base de datos de
choique en el databases.yml del proyecto de yavu:

```yml
development:
  adapter:  mysql2
  encoding: utf8
  database: yavu_development
  pool:     5
  username: root
  password:
  host:     localhost

test:
  adapter:  mysql2
  encoding: utf8
  database: yavu_test
  pool:     5
  username: root
  password:
  host:     localhost

production:
  adapter:  mysql2
  encoding: utf8
  database: yavu_production
  pool:     5
  username: root
  password: 
  host:     localhost

choique:
  development:
    adapter:  mysql2
    encoding: utf8
    database: choique_unlp
    pool:     5
    username: root
    password:
    port: 3307
    host:     127.0.0.1
```

Para poder migrar de choique a Yavu, necesitamos tener disponible:

* La base de datos
* Los archivos de medios y documentos

Entonces es necesario cargar la base de datos de choique en una base de datos temporal o apuntar
directamente a la base de producción, y luego copiar los archivos de choique en
la carpeta `YAVU_BASE/data/choique/migrate/{assets,docs}`

Para este paso, se provee de la siguiente tarea:

```bash

rake choique:sync[user@choique.server,/opt/applications/choique/current/web-backend/uploads]

```

Por último es necesario correr una migración que agrega una tabla intermedia temporal donde se almacenarán los ids de los artículos de choique con su correspondencia en Yavu.

```bash
rake db:migrate

```


## La migración

Para agregar en Yavu los medios, artículos y secciones, correr la siguiente
tarea rake:

```bash
rake choique:migrate
```

La tarea admite los siguientes argumentos:

* Fuente de noticias: si no se especifica, se asume `choique`
* Suplemento: si no se especifica, se asume `choique`
* Limites de migración: *(solo para pruebas)* especifica la cantidad de
  artículos a migrar en cada sección, así como cantidad de medios a migrar. Deja
  una migración incompleta, pero donde no demora demasiado y puede verificarse si
  la migración será exitosa

**La migración es incremental, por lo que pueden hacerse pruebas con valores del
limite que sea incremental que los datos cargados se mantienen y solo agregan
nuevas entidades**

### Ejemplo de uso de argumentos

```bash
rake choique:migrate[fuente-choique,suplemento-choique,10]
```

_En este caso se migrarán los articulos y secciones asociados a la fuente de
noticias **fuente-choique**, en el suplemento **suplemento-choique** y sólo se migrarán
10 medios/artículos_


## Relacionando y modificando los datos

Una vez migrados los datos necesitamos crear las relaciones entre ellos. Esto significa que a cada artículo se le asociarán los medios que le corresponden. 

```bash
  rake choique:relate
```

Por último debemos readapatar el cuerpo de los distintos artículos para que sean acordes a Yavu. Esto significa modificar los enlaces a elementos de choique (medios, artículos) por enlaces a elementos en Yavu.

```bash
  rake choique:readapt
```

Ambas tareas admiten como único argumento opcional un *límite*, es decir, cuántos artículos se veran afectados luego de correr las tareas. 


### Ejemplo de uso de argumentos

```bash
  rake choique:relate[10]
```
